package nl.utwente.di.temperature;

public class Converter {
    double getTemperature(int temp){
        return ((double) (temp * 9) / 5) + 32;
    }
}

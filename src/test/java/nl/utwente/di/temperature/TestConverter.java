package nl.utwente.di.temperature;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class TestConverter {

    @Test
    public void testBook1() throws Exception {
        Converter converter = new Converter();
        double temp = converter.getTemperature(1);
        Assertions.assertEquals(33.8, temp, 0.0);
    }
}
